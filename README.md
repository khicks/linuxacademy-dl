# Downloader for Linux Academy

This script will download courses from [Linux Academy](https://linuxacademy.com) for offline consumption.

## Important Notice

**Use of this script is for personal consumption of content only.** The content this script downloads is protected by copyright and must not be shared.

#### Good uses

* Downloading a lesson before embarking to a destination with little or no internet access.
* Keep lessons you've completed for a personal backup.

#### Bad uses

* Uploading the downloaded videos to a content sharing site.
* Sending copies of the videos to your friends and family.
* Hoarding lessons for future consumption beyond your subscription period.

Please exercise good judgement when using this script. The folks at Linux Academy work hard to make quality courses and you should support them by paying for a subscription if you can. You may also wish to speak with your employer to find out if they would be willing to pay for your subscription.

## Installation

Tested on a fresh install of Ubuntu 18.04 desktop. Your mileage may vary depending on your OS.

    sudo apt update
    sudo apt install python3 python3-pip git unzip ffmpeg
    sudo pip3 install selenium youtube-dl
    
### Browser

You will need Chrome or Firefox and its matching driver.

#### Chrome

Install [Google Chrome](https://www.google.com/chrome/) and download the appropriate [ChromeDriver](https://chromedriver.chromium.org/downloads) version. Make sure the `chromedriver` executable is in your PATH (e.g., `/usr/local/bin`).

    sudo dpkg -i google-chrome-stable_current_amd64.deb
    unzip chromedriver_linux64.zip
    sudo mv chromedriver /usr/local/bin
    
#### Firefox

Install Mozilla Firefox and download [geckodriver](https://github.com/mozilla/geckodriver/releases).

    sudo apt install firefox
    tar xzf geckodriver-*-linux64.tar.gz
    sudo mv geckodriver /usr/local/bin

## Usage

This will only work with an active Linux Academy subscription. If you do not have one, please get one [here](https://linuxacademy.com/pricing/).

    Usage:
        ./linuxacademy-dl.py [-u|--username] [-p|--password] [-d|--download-dir] [-c|--cookies-file] COURSE_URL
    
        Options may be replaced with environment variables. Command line options take precedence.
            LADL_USERNAME, LADL_PASSWORD, LADL_DIR, LADL_COOKIES
    
    Examples:
        $ ./linuxacademy-dl.py -u person@exmple.com -p p@ssw0rd https://linuxacademy.com/cp/modules/view/id/346
    
        $ export LADL_USERNAME=person@example.com
        $ export LADL_PASSWORD=p@ssw0rd
        $ export LADL_DIR=/home/jdoe/linux-academy
        $ ./linuxacademy-dl.py https://linuxacademy.com/cp/modules/view/id/346

The username/email and password fields are required. The cookie file will opt to `$PWD/cookies.txt` and the download directory will default to `$SCRIPT_DIR/download`, where `$SCRIPT_DIR` is the path of the `linuxacademy-dl.py` script.

## Attribution

Major thanks to [Vassim Shahir](https://libraries.io/github/vassim/linuxacademy-dl) and [Sam Hagin](https://github.com/samhagin/linuxacademy-dl) for their previous work that inspired this project.